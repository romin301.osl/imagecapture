import React, { useState, useEffect } from "react";
import "../css/firstpage.css";
// import { useFormik } from "formik";
import "bootstrap-icons/font/bootstrap-icons.css";
import cemeraIcon from "../icons/dslr-camera.png";
// import camera from "bootstrap-icons"
import "font-awesome/css/font-awesome.min.css";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
var Buffer = require("buffer/").Buffer;
function ForImage() {
  // const formik = useFormik({
  //   initialValues: {
  //   },
  //   onSubmit: async (values) => {
  //   },
  // });
  const [imageData, setImageData] = useState({});
  const postImageData = async (imageData) => {
    // DownloadCanvasAsImage()
    console.log(imageData);
    const image_id = "Img1";
    try {
      const res = await fetch("/postImageCaptureData", {
        method: "POST",
        headers: { "Content-Type": "application/json" },
        body: JSON.stringify({
          image_id,
          image_base64_data: imageData,
        }),
      });

      const data = await res.json();

      if (res.status === 400 || res.status === 422 || !data) {
        window.alert("Invalid");
      } else {
        console.log("data postSuccessful");
      }
    } catch (error) {
      console.log(error);
    }
  };

  const getImageBase64Data = async () => {
    try {
      const res = await fetch("/getImageData", {
        method: "GET",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
        },
        credentials: "include",
      });

      const data = await res.json();
      console.log(data);
      setImageData(data);

      if (res.status === 400 || res.status === 422 || !data) {
        return res.status(422).send("Data not recieved !!!");
      }
    } catch (error) {
      console.log("No data found ( Unauthorized ) !!!");
    }
  };
  // useEffect(() => {
  //   getImageBase64Data();
  // }, []);
  navigator.usb.getDevices().then((devices) => {
    console.log(`Total devices: ${devices.length}`);
    devices.forEach((device) => {
      console.log(
        `Product name: ${device.productName}, serial number ${device.serialNumber}`
      );
    });
  });
  // navigator.usb.getDevices({ filters: [] }).then(function (device) {
  //   console.log("hello",device);
  // });



  // navigator.usb
  //   .requestDevice({ filters: [{ vendorId: 4e22 }] })
  //   .then((device) => {
  //     console.log(device.productName); // "Arduino Micro"
  //     console.log(device.manufacturerName); // "Arduino LLC"
  //   })
  //   .catch((error) => {
  //     console.error(error);
  //   });
 

  const check = () => {
    const filters = [
      // { vendorId: 0x1209, productId: 0xa800 },
      // { vendorId: 0x1209, productId: 0xa850 },
      
    ];
    navigator.usb
      .requestDevice({ filters })
      .then((usbDevice) => {
        console.log(`Product name: ${usbDevice.productName}`);
      })
      .catch((e) => {
        console.error(`There is no device. ${e}`);
      });
  }

  const checkDevice = () => {
    // console.log("Checking !!!!")
    "mediaDevices" in navigator && "getUserMedia" in navigator.mediaDevices
      ? console.log("Media device cheking !!!")
      : console.log("not supported");
    const x = navigator.mediaDevices.getUserMedia({ video: true });
    const canvas = document.querySelector("#canvasid");
    const video = document.querySelector("#videoid");
    //setup video constraints like height , width, min, max, ideal, exact, facingMode
    const constraints = {
      video: {
        width: {
          min: 400,
          ideal: 500,
          max: 400,
        },
        height: {
          min: 400,
          ideal: 500,
          max: 400,
        },
        facingMode: "user",
      },
    };
    // console.log(x);
    //requested for video permission
    const forPermission = async () => {
      try {
        const x = await navigator.mediaDevices.getUserMedia(constraints);
        console.log(x);
        video.srcObject = x;
        // if(x)
        // {
        //   console.log("Permission accepted")
        // }else{
        //   console.log("denied")
        // }
        // afterCameraEnable();
      } catch (error) {
        document.getElementById("noAnyCemeraDevice").innerHTML =
          "Device not found";
        console.log(error);
      }
    };
    forPermission();
    //get device ID
    async function getDevices() {
      const devices = await navigator.mediaDevices.enumerateDevices();
      console.log(devices);
    }
    getDevices();
    //for displaying capture image
    const screenshotContainer = document.querySelector("#screenshotContainer");
    const captureImage = document.querySelector("#takeScreenshot");
    // const afterCameraEnable = () => {
    captureImage.addEventListener("click", () => {
      // const img = document.createElement("img");
      canvas.width = video.videoWidth;
      canvas.height = video.videoHeight;
      canvas.getContext("2d").drawImage(video, 0, 0);
      video.srcObject.getVideoTracks().forEach((track) => {
        track.stop();
      });
      // img.src = canvas.toDataURL();
      // screenshotContainer.prepend(img);
    });
    // }
  };
  const canvas1 = document.querySelector("#canvasid");
  const video1 = document.querySelector("#videoid");
  // function DownloadCanvasAsImage() {
  //   console.log("*****");
  //   // let downloadLink = document.createElement('a');
  //   // downloadLink.setAttribute('download', 'CanvasAsImage.png');
  //   let canvas = document.getElementById("canvasid");
  //   canvas.width = video1.videoWidth;
  //   canvas.height = video1.videoHeight;
  //   canvas.getContext("2d").drawImage(video1, 0, 0);
  //   let dataURL = canvas.toDataURL("image/png");
  //   let url = dataURL.replace(/^data:image\/(png|jpg);base64,/, "");
  //   localStorage.setItem("imgData", url);
  //   console.log(url);
  //   return url;
  //   // downloadLink.setAttribute('href',url);
  //   // downloadLink.click();
  // }
  // function DownloadCanvasAsImage(){
  //   let downloadLink = document.createElement('a');
  //   downloadLink.setAttribute('download', 'CanvasAsImage.png');
  //   let canvas = document.getElementById('canvasid');
  //   canvas.toBlob(function(blob) {
  //     let url = window.URL.createObjectURL(blob);
  //     downloadLink.setAttribute('href', url);
  //     downloadLink.click();
  //   });
  // }

  let image64data;
  function DownloadCanvasAsImage() {
    let downloadLink = document.createElement("a");
    downloadLink.setAttribute("download", "CanvasAsImage.png");
    let canvas = document.getElementById("canvasid");
    let dataURL = canvas.toDataURL("image/png");
    console.log(dataURL);
    let url = dataURL.replace(
      /^data:image\/png/,
      "data:application/octet-stream"
    );
    image64data = url;
    // dataURL.replace(/^data:image\/(png|jpg);base64,/, "");
    const buffer = Buffer.from(image64data, "base64");
    const bufString = buffer.toString("binary");
    console.log(bufString);

    console.log(url);
    // console.log(imageData);
    downloadLink.setAttribute("href", url);
    downloadLink.click();
    video1.srcObject.getVideoTracks().forEach((track) => {
      track.stop();
    });
    // function Base64ToImage(image64data, callback) {
    //   var img = new Image();
    //   img.onload = function () {
    //     callback(img);
    //   };
    //   img.src = image64data;
    //   // img.height = "300px"
    //   // img.width = "300px"
    // }
    // Base64ToImage(image64data, function (img) {
    //   // const screenshotContainer1 = document.querySelector("#screenshotContainer1");
    //   console.log(image64data);
    //   document.getElementById("screenshotContainer1").appendChild(img);
    //   // const img = document.createElement("img");
    //   // screenshotContainer1.prepend(img);
    // });

    postImageData(dataURL);

    // screenshotContainer1.prepend(img1);
    // window.location.reload();

    //sayaib image base64 to image
    //   let rowImages=[]
    // image64data.map(function (val, key) {
    //   console.log(`${key}:${val}`);
    //   console.log(val);
    //   let input = val.replace(/[^A-Fa-f0-9]/g, "");
    //   console.log(input);
    //   if (input.length % 2) {
    //     console.log("cleaned hex string length is odd.");
    //     return;
    //   }
    //   let binary = new Array();
    //   for (let i = 0; i < input.length / 2; i++) {
    //     let h = input.substr(i * 2, 2);
    //     // console.log(h);
    //     binary[i] = parseInt(h, 16);
    //   }
    //   let byteArray = new Uint8Array(binary);
    //   let image64dataRow = rowImages.push(
    //     new Blob([byteArray], {
    //       type: "application/octet-stream",
    //     })
    //   );
    //   console.log(byteArray);
    //   let img = document.querySelector(".heximage");
    //   rowImages.map((val) => {
    //     return val;
    //   });
    //   // // console.log(binary);
    //   img.src = window.URL.createObjectURL(
    //     new Blob([byteArray], { type: "application/octet-stream" })
    //   );
    // });
  }
  // const download_img = () => {
  //   document.getElementById("download").download = "image.png";
  //   document.getElementById("download").href = document
  //     .getElementById("canvasid")
  //     .toDataURL("image/png")
  //     .replace(/^data:image\/png/,'data:application/octet-stream');
  // };
  function download() {
    var download = document.getElementById("download");
    var image = document
      .getElementById("canvasid")
      .toDataURL("image/png")
      .replace("image/png", "image/octet-stream");
    download.setAttribute("href", image);
  }
  return (
    <>
      <div className="createNewPassword">
        <button onClick={check}>Click me</button>
        <div className="main">
          <div className="wrapper">
            <div className="content">
              {/* <div style={{ textAlign: "left", marginLeft: "0.5rem" }}>
                <a href="" style={{ textDecoration: "none", color: "black" }}>
                  <KeyboardBackspaceIcon style={{ marginRight: "0.2rem" }} />
                  Back
                </a>
              </div> */}
              <div className="loginForm">
                <h4>Image Capture And Display</h4>
                {/* <p>
                  Your new password must be different then the previous one.
                </p> */}
                <button onClick={checkDevice} className="btn">
                  Click here to start camera
                </button>
                <br />
                {/* <h3 className="closeIcon" id="noAnyCemeraDevice"></h3> */}
                {/* <a id="download" download="myImage.jpg" href="#" onClick={download_img}>Download to myImage.jpg</a> */}
                <button onClick={DownloadCanvasAsImage} className="btn2">
                  Download and Display Image
                </button>
                <br />
                <button onClick={getImageBase64Data} className="btn1">
                  Show capture Image
                </button>
                {/* <a id="download" download="image.png"><button type="button" onClick={download}>Download</button></a> */}
              </div>
            </div>
            <div className="content1">
              <button
                className="bi bi-camera-fill"
                id="takeScreenshot"
              ></button>
              <p style={{ textAlign: "center" }}>
                Click here for capture image
              </p>
              <div>
                <video autoPlay id="videoid"></video>
                <canvas id="canvasid"></canvas>
                {/* <div className="content" id="screenshotContainer"></div> */}
              </div>

              <div>
                <img src={imageData.image_base64_data} alt="" />
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
export default ForImage;
