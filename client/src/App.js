import logo from './logo.svg';
import './App.css';
import ImageCapture from "../src/pages/images"
import { Routes, Route, } from "react-router-dom";

function App() {
  return (
    <>
      <Routes>
        <Route path="/" element={<ImageCapture/>}></Route>
      </Routes>
    </>
  );
}

export default App;
