const express = require('express');
const app = express();
const path = require('path');
require('./db/conn')

// app.use(express.json())
app.use(express.json({limit: '1024mb'}));

// app.use(express.json({limit: '50mb'}));
// app.use(express.urlencoded({limit: '50mb'}));
app.use(express.static(path.join(__dirname, 'captureImage')));

const ImageCapture = require(path.join(__dirname, './model/imageUrl'))

app.use(require('./controller/auth'));

const PORT = 9000;

app.listen(PORT, () => {
    console.log(`server is running in port ${PORT} `)
})