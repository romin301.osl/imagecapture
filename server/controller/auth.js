const e = require('express');
const express = require('express')
require('../db/conn')
const path = require('path');
const fs = require('fs');
const router = express.Router();
const ImageCapture = require('../model/imageUrl')
var Buffer = require('buffer/').Buffer 

const timeStamp = () => {
    let date = new Date();
    const year = date.getFullYear(); // 2019
    const month = date.getMonth() + 1;
    const day = date.getDate(); // 23
    let getTime = date.toLocaleTimeString();
  
    // console.log(`${day}/${month}/${year} - ${getTime}`);
    // return `${day}-${month}-${year}-${getTime}`;
    return `${day}-${month}-${year}`;
    
  };

//post image base64 data and it's id
router.post('/postImageCaptureData', async (req, res) => {
    try {
        const { image_id, image_base64_data } = req.body;
        // console.log(image_base64_data)
        let divideBase64 = image_base64_data.match(/^data:([A-Za-z-+\/]+);base64,(.+)$/);
        // console.log(divideBase64[1]);
        let imageBufferData = new Buffer(divideBase64[2], 'base64');
        // console.log(imageBufferData)
        const imagePath = path.join(__dirname, '../captureImage/')
        // console.log(imagePath)
        let fileName = `Image-${timeStamp()}`;
        let imageFullPathAndName = imagePath + fileName + '.png'
        // console.log(imageFullPathAndName);
        try{
            fs.writeFile(imageFullPathAndName,imageBufferData,function(error){
                console.log(error);
            })
        }catch(error){
            console.log(error);
        }
        let fileNameInDb = fileName + ".png";

        if(!image_id || !image_base64_data)
        {
            return res.status(422).json({ error: 'No any detail received' })
        }
        const check = await ImageCapture.findOne({image_id:"Img1"});
        if(check){
            const updateImage = await ImageCapture.updateOne({image_id:"Img1"},{$set:{image_base64_data:fileNameInDb}})
            // console.log(updateImage);
        }
        else{
            const result = new ImageCapture({image_id,image_base64_data:fileNameInDb})
            await result.save();
        }

        res.status(201).json({ message: " Image data store successfully" })
        // console.log(image_id, image_base64_data);

    } catch (error) {
        console.log(error);
    }
})

//get image data and display it
router.get('/getImageData', async (req, res) => {
    try {
        const getImage = await ImageCapture.findOne({ image_id: "Img1" });
        res.send(getImage)
    } catch (error) {
        console.log("Image data not found");
    }

})

module.exports = router;