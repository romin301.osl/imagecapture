const mongoose = require('mongoose');

//Create log schema for collection document
const imageSchema = new mongoose.Schema({
    image_id:{
        type: String,
    },
    image_base64_data: {
        type: String,
    }
})

//Here crete Log collection for storing logs of user documents.
const ImageCapture = new mongoose.model("ImageCaptureDatas", imageSchema);

module.exports = ImageCapture;